import math
import numpy as np


def gaussian_bivar(x, mi, det, inv):

    # print('what is det? ', det)
    term1 = 2 * math.pi * math.sqrt(det)  # negative determinants cause math error
    # term1 = 2.0 * math.pi * det ** (1/2.0)

    # dif = np.transpose(np.matrix(np.subtract(x, mi)))
    dif = np.subtract(x, mi)

    dif_t = np.transpose(dif)
    mult = np.dot(dif_t, inv).dot(dif)
    div = np.divide(mult, -2)
    term2 = math.exp(div)

    return 1 / term1 * term2


class EM:

    # returns the covariance matrices for a list of given centroids
    def covariance(self, means):

        covars = []

        for i in range(len(means)):
            covar = np.zeros((2, 2))

            for x in self.data:  # x is (float, float)
                j = self.data.index(x)

                if self.gama[j][i] == 0.0:
                    continue

                # difference between x and the centroid
                dif = np.transpose(np.matrix(np.subtract(x, means[i])))

                # transposed of the difference
                dif_t = np.transpose(np.matrix(dif))

                # product of the difference and its transposed
                mult = np.dot(dif, dif_t)  # k by k matrix

                # multiply by gama of n on k
                wghtnd = np.dot(mult, self.gama[j][i])

                # accumulated products
                covar = np.add(covar, wghtnd)

            # dividing by component cardinality
            covars.append(np.divide(covar, self.card[i]))

        return covars

    def mix_coeff(self):
        return np.divide(self.card, float(len(self.data)))

    def __init__(self, data):
        self.data = data
        self.gama = []
        self.card = []
        self.sigma = []
        self.det = []
        self.inv = []
        self.pi = []

    def weigh_distribution(self, pi, xn, mean, det, inv):
        return pi * gaussian_bivar(xn, mean, det, inv)

    def log_likelihood(self, means):

        lklhood = 0.0
        self.det = []
        self.inv = []

        for k in self.sigma:
            self.det.append(np.linalg.det(k))
            self.inv.append(np.linalg.inv(k))

        # sum over all x
        for x in self.data:
            sum_joint = 0.0
            # of the log of the sum over all k
            for i in range(len(self.pi)):
                # of the responsibility self.pi
                # multiplied by the gaussian distribution -> gaussian_bivar(x, mi, self.det, self.inv)
                sum_joint += self.weigh_distribution(self.pi[i], x, means[i], self.det[i], self.inv[i])
            lklhood += math.log(sum_joint)

        return lklhood

    # updates gama matrix
    def expectation(self, means):

        # for each datum
        for x in self.data:
            i = self.data.index(x)

            num = []
            den = 0.0

            for j in range(len(self.pi)):
                term = self.weigh_distribution(self.pi[j], x, means[j], self.det[j], self.inv[j])
                num.append(term)
                den += term
            # print('num =', num)

            for j in range(len(self.pi)):
                self.gama[i][j] = num[j] / den

    def fix_cardinalities(self):
        # print('antes =', self.card)

        new_card = [0.0, 0.0]

        for i in range(len(self.data)):  # for each datum
            for j in range(len(self.card)):  # for each class
                new_card[j] += self.gama[i][j]

        self.card = new_card

        # print('depois =', self.card)

    # updates the parameters
    def maximization(self):

        self.fix_cardinalities()

        # new means
        means = []

        # new mean of each class
        for j in range(len(self.card)):
            parc_sum = [0.0, 0.0]
            # sum for all datum
            for x in self.data:
                i = self.data.index(x)
                # the product of gama and x
                prod = np.dot(self.gama[i][j], x)
                parc_sum = np.add(parc_sum, prod)
            means.append(tuple(np.divide(parc_sum, self.card[j])))

        # print('means =', means)

        self.sigma = self.covariance(means)
        self.pi = self.mix_coeff()

        return means

    def cluster(self, means, gama, card, precision):
        convergence = 0
        it = 0

        self.gama = gama
        self.card = card
        self.sigma = self.covariance(means)
        self.pi = self.mix_coeff()

        likelihood = self.log_likelihood(means)

        # stopping conditions? iterations, likelihood
        while not convergence:
            it += 1
            # print('likelihood =', likelihood)  # changes are so small

            self.expectation(means)
            new_means = self.maximization()
            new_likelihood = self.log_likelihood(new_means)

            # if new_likelihood == likelihood:
            # if set(means) == set(new_means):

            if abs(new_likelihood - likelihood) < 0.1 ** precision:
                convergence = 1
            else:
                likelihood = new_likelihood
                means = new_means
            # how to verify the convergence of the parameters?

        print('\nE-M iterations: ', it)
        for i in range(len(new_means)):
            print('\tCluster[' + str(i + 1) + ']')
            print('\tMean:\n\t\t(', ', '.join(['{0:.4f}'.format(x) for x in new_means[i]]), ')')
            print('\tCovar matrix:')
            for l in self.sigma[i]:
                print('\t\t', l)
            print('\tMixture coef:', '{0:.4f}'.format(self.pi[i]))

        return new_means, self.gama
