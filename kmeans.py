import math
import random

import numpy as np


def euclidean_2d(x, m):
    return math.sqrt((x[0] - m[0])**2 + (x[1] - m[1])**2)


def most_likely(x, means):
    smst = euclidean_2d(x, means[0])
    nrst = 0
    for m in range(1, len(means)):
        dist = euclidean_2d(x, means[m])
        if dist < smst:
            smst = dist
            nrst = m
    return nrst


class KMeans:

    def __init__(self, data, k=2):
        self.k = k
        self.data = data

    def init_means(self, method):
        # if method == 0:
        centroids = random.sample(self.data, self.k)
        # else
        # other methods: average 3 executions of k-means
        return centroids

    def distribute(self, means):
        clusters = []
        card = []
        k = len(means)

        for c in range(k):
            card.append(0.0)

        for x in self.data:
            # setting most likely class to 1
            pertains_to = most_likely(x, means)
            k_values = []
            for c in range(k):
                k_values.append(0.0)
            k_values[pertains_to] = 1.0
            card[pertains_to] += 1.0
            clusters.append(k_values)

        return clusters, card  # gama and cardinality of the clusters

    def update_means(self, clusters):

        n = len(clusters)
        means = []
        count = []
        accum_x = []
        accum_y = []

        for k in clusters[0]:
            count.append(0)
            accum_x.append(0.0)
            accum_y.append(0.0)

        for i in range(n):
            max_class = np.argmax(clusters[i])
            count[max_class] += 1
            accum_x[max_class] += self.data[i][0]
            accum_y[max_class] += self.data[i][1]

            # term_x = accum_x[max_class] + self.data[i][0]  # x
            # term_y = accum_y[max_class] + self.data[i][1]  # y
            # accum_x[max_class] = round(term_x, 4)
            # accum_y[max_class] = round(term_y, 4)

        for i in range(len(count)):
            pi = count[i]
            coord_x = accum_x[i]/pi
            coord_y = accum_y[i]/pi
            means.append((coord_x, coord_y))

        return means

    def cluster(self, method=0):

        convergence = 0
        it = 0

        means = self.init_means(method)
        # print('means =', means)

        while not convergence:
            it += 1
            clusters, card = self.distribute(means)
            new_means = self.update_means(clusters)
            # print('new_means =', new_means)
            if set(means) == set(new_means):
                convergence = 1
            else:
                means = new_means

        print('\nK-means iterations:', it)

        for i in range(len(new_means)):
            print('\tCluster', i + 1, 'mean:\n\t\t', end='')
            print('(', ', '.join(['{0:.4f}'.format(x) for x in new_means[i]]), ')')

        return means, clusters, card
