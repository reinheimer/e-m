import sys
import random
from kmeans import KMeans
from mixem import EM

import plotly as py
import plotly.graph_objs as go


def graph(data, means, clusters, title, file='./temp.html'):
    random_colors = []
    dots = []
    layout = go.Layout(title=title)

    hue = [0, 2]

    for i in range(len(means)):
        random_color = 'rgba('

        for c in range(3):
            if c == hue[i]:
                random_color += str(int(random.gauss(250, c * 15.0))) + ', '
            else:
                # random_color += str(random.randint(0, int(250/(len(means) - i)))) + ', '
                random_color += str(int(random.gauss(0.25 * 250, 10.0))) + ', '
        random_colors.append(random_color)

        dots.append(go.Scatter(x=[means[i][0]], y=[means[i][1]], mode='markers',
                               name='Centroid '+str(i+1), marker=dict(color=random_colors[i] + '.9)', size=10)))

    group_x1 = [x for (x, y) in data if clusters[data.index((x, y))][0] > clusters[data.index((x, y))][1]]
    group_x2 = [x for (x, y) in data if clusters[data.index((x, y))][0] <= clusters[data.index((x, y))][1]]
    group_y1 = [y for (x, y) in data if clusters[data.index((x, y))][0] > clusters[data.index((x, y))][1]]
    group_y2 = [y for (x, y) in data if clusters[data.index((x, y))][0] <= clusters[data.index((x, y))][1]]

    dots.append(go.Scatter(x=group_x1, y=group_y1,
                           name='Group 1', mode='markers', marker=dict(color=random_colors[0] + '.6)')))

    dots.append(go.Scatter(x=group_x2, y=group_y2,
                           name='Group 2', mode='markers', marker=dict(color=random_colors[1] + '.6)')))

    fig = go.Figure(data=dots, layout=layout)
    py.offline.plot(fig, filename=file)


with open("./dist2.txt", 'r') as f:
    points = []
    for line in f:
        n1, n2 = (float(s) for s in line.split() if line.strip())
        points.append((n1, n2))

    km = KMeans(points)
    mi_k, cl_k, n_k = km.cluster()
    graph(points, mi_k, cl_k, 'K-Means', 'kmeans.html')

    em = EM(points)
    mi_em, cl_em = em.cluster(mi_k, cl_k, n_k, 6)
    graph(points, mi_em, cl_em, 'E-M algorithm', 'em.html')

sys.exit(0)
